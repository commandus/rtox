# RTox

Android Tox chat client. 

This is alpha version, do not use this software in production.

Idea is share documents and "actions" using P2P explained in the [White Paper (draft)] (https://docs.google.com/document/d/1SYbSWyqOKCRC6haHpBfBro7_IADL-4Pvaz4quuilO5A/edit?usp=sharing)

Smartphone receive/send text/image/other docs from registered addresses in the white list.

## Related projects

### clitox

Server (Linux) side: Simple command line interface (CLI) TOX client send text/action notifications to group of addresses. 

- [Repository git@github.com:commandus/clitox.git](https://github.com/commandus/clitox.git)
- [Site https://github.com/commandus/clitox](https://github.com/commandus/clitox)

## JNI
```
            +-----------------+
            | createClient    |
            +------+----------+
                   | new descriptor
            +------+----------+
+---------->|  ToxClient C    +-----------------------+
|  send     +------+----------+      event callbacks  |
|  message         |                                  |
|           +------+----------+            +----------v-----------+
|           |  ToxClient Java +^-----------+ ToxReceiverInterface |
|           +-----------------+            +----------------------+
+-----------+ putMessage      |            | onId                 |
            +-----------------+            | onConnectionStatus   |
                                           | onMessage            |
                                           | onFriendRequest      |
                                           +----------------------+

```
## Dependencies

No license type is assigned to libtoxcore. libtoxcore is fork of [irungentoo/toxcore] (https://github.com/irungentoo/toxcore).

| Library              | License types                                                              |
|----------------------|----------------------------------------------------------------------------|
| libsodium            | [ISC] (https://en.wikipedia.org/wiki/ISC_license)                          |
| libtoxcore           | [GPL v3] (https://wikipedia.org/wiki/GNU_General_Public_License#GPL_v3)    |

### Building dependencies

#### libsodium

```
cd ~/lib/Robosodium
export ANDROID_NDK_HOME=~/Android/Sdk/ndk-bundle/
export SODIUM_HOME=~/lib/libsodium/
export NDK_PLATFORM="android-26"
export NDK_PLATFORM_COMPAT="android-21"
./build_android_libsodium_osx.sh

cp ~/lib/Robosodium/libsodium/libsodium-android-armv6/lib/libsodium.so ~/src/rtox/app/imported-lib/sodium/armeabi

# If needed
cp ~/lib/Robosodium/libsodium/libsodium-android-armv7-a/lib/libsodium.so ~/src/rtox/app/imported-lib/sodium/armeabi-v7a
cp ~/lib/Robosodium/libsodium/libsodium-android-mips32/lib/libsodium.so ~/src/rtox/app/imported-lib/sodium/mips
cp ~/lib/Robosodium/libsodium/libsodium-android-i686/lib/libsodium.so ~/src/rtox/app/imported-lib/sodium/x86

```

#### libtoxcore

```
cd ~/lib/c-toxcore
export ANDROID_NDK_HOME=~/Android/Sdk/ndk-bundle/
export SODIUM_HOME=~/lib/libsodium/
export NDK_PLATFORM="android-26"
export NDK_PLATFORM_COMPAT="android-21"
dist-build/android-arm.sh
...
cp ~/lib/c-toxcore/toxcore-android-arm/lib ~/src/rtox/app/imported-lib/c-toxcore/armeabi
cp ~/lib/c-toxcore/toxcore-android-mips/lib ~/src/rtox/app/imported-lib/c-toxcore/mips
cp ~/lib/c-toxcore/toxcore-android-x86/lib ~/src/rtox/app/imported-lib/c-toxcore/x86
```

#### Building JNI

```
cd ~/src/rtox/app/src/main/cpp/sodium
swig -java -c++ -package org.abstractj.sodium -o sodiumJAVA_wrap.cxx -outdir ../../../../src/main/java/org/abstractj/sodium/ sodium.i

cd ~/src/rtox/app/src/main/cpp/toxcore
swig -java -c++ -package org.irungentoo.toxcore -o toxcoreJAVA_wrap.cxx -outdir ../../../../src/main/java/org/irungentoo/toxcore/ toxcore.i

```

## Links

https://github.com/GerardSoleCa/Robosodium

## License

GNU General Public License v3
