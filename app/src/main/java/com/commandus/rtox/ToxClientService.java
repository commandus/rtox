package com.commandus.rtox;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.commandus.data.BootstrapNode;
import com.commandus.data.Helper;
import com.commandus.data.ToxMessage;
import com.commandus.provider.MessageProvider;
import com.commandus.tox.ToxClientRunner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Application service receives Tox request messages from the activities and handle them in {@link IncomingHandler}.
 * A new {@link AsyncTask} waits web service response, then broadcast notification back by {@link #sendMessageToClients(int, int, Bundle)}
 * <br/>
 * Voice. {@link #say(String, String)}
 * Get static application context {@link #getContext()} e.g. Context ctx = ToxClientService.getContext();
 */
public class ToxClientService
        extends Service
        implements TextToSpeech.OnInitListener, ToxReceiverInterface
{
    private static final String TAG = ToxClientService.class.getSimpleName();

    private static final String URL_BOOTSTRAP_NODES = "https://nodes.tox.chat/json";

    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final int MSG_COMMAND = 3;
    public static final int MSG_SEND_MESSAGE = 4;
    public static final int MSG_START_SESSION = 5;
    public static final int MSG_FINISH_SESSION = 6;

    public static final int CMD_STATUS = 1;
    public static final int CMD_FRIEND_REQUEST = 2;
    public static final int CMD_MESSAGE = 3;
    public static final int CMD_INTENT = 4;
    public static final int CMD_NEXT_MESSAGE_TO = 5;

    public static final String KEY_NUMBER = "i";
    public static final String KEY_ID = "key";
    public static final String KEY_CN = "cn";
    public static final String KEY_VALUE = "v";
    static final String KEY_TYPE = "t";
    public static final String KEY_CLIENT = "c";

    private ApplicationSettings mAppSettings;

    // every minute do smth
    private static final long TIME_INTERVAL_CHECK_FOREGROUND = TimeUnit.SECONDS.toMillis(60);

    final Messenger mMessenger = new Messenger(new IncomingHandler());
    static private List<Messenger> mClients = new ArrayList<>();

    private static android.speech.tts.TextToSpeech mTTS;

    private static ToxClientService mInstance;
    public static Context getContext() {
        return mInstance.getApplicationContext();
    }

    private static Thread mThreadClient;
    private static ToxClientRunner mToxClientRunner;
    public static ToxClientService getInstance() {
        return mInstance;
    }

    public ToxClientRunner getToxClientRunner() {
        return mToxClientRunner;
    }

    public static String getToxId() {
        if (mThreadClient != null && mThreadClient.isAlive() && mToxClientRunner != null) {
            ToxClient c = mToxClientRunner.getToxClient();
            if (c != null) {
                return c.getId();
            }
        }
        return "";
    }

    /**
     * Set user name
     * @param value user name
     */
    public void setUserCN(String value) {
        ApplicationSettings settings = ApplicationSettings.getInstance(this);
        if (settings != null)
            settings.setUserCN(this, value);
    }

    public ToxClientService() {
        mInstance = this;
        Log.i(TAG, "Creating rtox service");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAppSettings = ApplicationSettings.getInstance(this);
        if (mAppSettings != null) {
            mAppSettings.setTTSRun(this, false);
            if (mAppSettings.isTtsEnabled()) {
                startTTS();
            }
        }
        Log.i(TAG, "Service created " + this.toString());
        startClient(ApplicationSettings.getToxFileName(this), mAppSettings.getUserCN(),
            mAppSettings.getNoFriendRequest(),
            mAppSettings.getFriendRequestMessage(),
            mAppSettings.getOnlineStatus(),
            mAppSettings.getAwayStatus(),
            mAppSettings.getBusyStatus()
    );
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Service destroyed");
        Log.i(TAG, "Interrupt client thread");
        mThreadClient.interrupt();
        // TextToSpeech
        if (mTTS != null) {
            mTTS.shutdown();
            mTTS = null;
        }
        // stop being a foreground service
        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    /**
     * Called when we receive an Intent. When we receive an intent sent to us via startService(),
     * this is the method that gets called. So here we react appropriately depending on the
     * Intent's action, which specifies what is being requested of us.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            // Happens every day morning
            return START_STICKY_COMPATIBILITY;
        }

        // startClient(ApplicationSettings.getToxFileName(this), mAppSettings.getUserCN(), "online");

        String action = intent.getAction();
        if (action == null) {
            // that's normal, when user back from the media list activity
            return START_STICKY_COMPATIBILITY;
        }
        return START_STICKY;
    }

    private void startClient(final String filename, final String nick,
        final boolean no_friend_request,
        final String friend_request_message,
        final String onlineStatus,
        final String awayStatus,
        final String busyStatus
    ) {
        Log.i(TAG, "Start client thread");
        if (mThreadClient != null && mThreadClient.isAlive())
            return;
        updateBoostrapNodes(filename, nick, getString(R.string.msg_start),
                no_friend_request,
                friend_request_message, onlineStatus, awayStatus, busyStatus
);
        /*
        List<BootstrapNode> nodes = getDefaultBootStrapNodes();
        mToxClientRunner = new ToxClientRunner(ToxClientService.this, nodes, filename, nick, status);
        mThreadClient = new Thread(mToxClientRunner);
        mThreadClient.start();
        */
    }

    private List<BootstrapNode> getDefaultBootStrapNodes() {
        List<BootstrapNode> nodes = new ArrayList<>();
        // nodes.add(new BootstrapNode("84.237.104.57",              33445, "12FE34B0F82D92016C34CBF7D80CBA3627AC0E7FD07CFBADC22BCA1EDD9CC75B"));
        nodes.add(new BootstrapNode("178.62.250.138",             33445, "788236D34978D1D5BD822F0A5BEBD2C53C64CC31CD3149350EE27D4D9A2F9B6B"));
        nodes.add(new BootstrapNode("2a03:b0c0:2:d0::16:1",       33445, "788236D34978D1D5BD822F0A5BEBD2C53C64CC31CD3149350EE27D4D9A2F9B6B"));
        nodes.add(new BootstrapNode("tox.zodiaclabs.org",         33445, "A09162D68618E742FFBCA1C2C70385E6679604B2D80EA6E84AD0996A1AC8A074"));
        nodes.add(new BootstrapNode("163.172.136.118",            33445, "2C289F9F37C20D09DA83565588BF496FAB3764853FA38141817A72E3F18ACA0B"));
        nodes.add(new BootstrapNode("2001:bc8:4400:2100::1c:50f", 33445, "2C289F9F37C20D09DA83565588BF496FAB3764853FA38141817A72E3F18ACA0B"));
        nodes.add(new BootstrapNode("128.199.199.197",            33445, "B05C8869DBB4EDDD308F43C1A974A20A725A36EACCA123862FDE9945BF9D3E09"));
        nodes.add(new BootstrapNode("2400:6180:0:d0::17a:a001",   33445, "B05C8869DBB4EDDD308F43C1A974A20A725A36EACCA123862FDE9945BF9D3E09"));
        nodes.add(new BootstrapNode("biribiri.org",               33445, "F404ABAA1C99A9D37D61AB54898F56793E1DEF8BD46B1038B9D822E8460FAB67"));
        return nodes;
    }

    private void startWithNodes(final String filename, final String nick, final String status,
        List<BootstrapNode> nodes, boolean no_friend_request,
        String friend_request_message, String onlineStatus, String awayStatus, String busyStatus
    ) {
        mToxClientRunner = new ToxClientRunner(ToxClientService.this, nodes, filename,
                nick, status,  no_friend_request,
                friend_request_message, onlineStatus, awayStatus, busyStatus);
        mThreadClient = new Thread(mToxClientRunner);
        mThreadClient.start();
    }

    private void updateBoostrapNodes(final String filename, final String nick, final String status,
                                     final boolean no_friend_request,
                                     final String friend_request_message,
                                     final String onlineStatus,
                                     final String awayStatus,
                                     final String busyStatus
    ) {
        try {
            AndroidNetworking.get(URL_BOOTSTRAP_NODES)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            List<BootstrapNode> nodes = new ArrayList<>();
                            try {
                                JSONArray jnodes = response.getJSONArray("nodes");
                                for (int i = 0; i < jnodes.length(); i++)
                                {
                                    JSONObject o = jnodes.getJSONObject(i);
                                    String ip = o.getString("ipv4");
                                    int port = o.getInt("port");
                                    String publicKey = o.getString("public_key");
                                    BootstrapNode node = new BootstrapNode(ip, port, publicKey);
                                    nodes.add(node);
                                }
                                startWithNodes(filename, nick, status, nodes, no_friend_request,
                                        friend_request_message, onlineStatus, awayStatus, busyStatus);
                            } catch (JSONException e) {
                                startWithNodes(filename, nick, status, getDefaultBootStrapNodes(),
                                        no_friend_request,
                                        friend_request_message, onlineStatus, awayStatus, busyStatus);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            startWithNodes(filename, nick, status, getDefaultBootStrapNodes(),
                                    no_friend_request,
                                    friend_request_message, onlineStatus, awayStatus, busyStatus);
                        }
                    });

        } catch (Exception e) {
            Log.e(TAG, "Bootstrap update list error: " + e.toString());
            e.printStackTrace();
            startWithNodes(filename, nick, status, getDefaultBootStrapNodes(),
                    no_friend_request,
                    friend_request_message, onlineStatus, awayStatus, busyStatus);
        }

    }

    /**
     * Handler of incoming messages from clients.
     */
    static class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    Log.d(TAG, "Register client " + msg.replyTo.toString());
                    if (mClients.contains(msg.replyTo))
                        Log.d(TAG, "Register client " + msg.replyTo.toString() + " already registered");
                    else
                        mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    Log.d(TAG, "Unregister client " + msg.replyTo.toString());
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_COMMAND:
                    Log.d(TAG, "Message?" + msg.replyTo.toString());
                    // TODO
                    break;
                case MSG_SEND_MESSAGE:
                    Bundle data = msg.getData();
                    int messageType = data.getInt(ToxClientService.KEY_TYPE);    // 0- text message, 1- action
                    int friendNumber = data.getInt(ToxClientService.KEY_NUMBER);
                    String body = data.getString(ToxClientService.KEY_VALUE);
                    int sent = (int) (new Date().getTime() / 1000L);
                    ToxMessage m = new ToxMessage(friendNumber, MessageProvider.OP_SENT, sent, "", "", messageType, body, 0);
                    addMessageToContentProvider(m);
                    if (mToxClientRunner != null) {
                        ToxClient c = mToxClientRunner.getToxClient();
                        if (c != null) {
                            c.putMessage(messageType, friendNumber, body);
                        } else {
                            Log.e(TAG, "Can  not send message- Tox client thread running, but no client provided.");
                        }
                    } else {
                        Log.e(TAG, "Can  not send message- Tox client is not running.");
                    }

                    Log.d(TAG, "Message to be send" + msg.replyTo.toString());
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Usage:
     *
     *  private void doSmth(final int msg) {
     Thread t = new Thread(new Runnable() {
     public void run() {
     // ...
     sendMessageToClients(msg, 0);
     }
     });
     t.start();
     }
     * @param arg1    reserved
     * @param arg2    reserved
     * @param data    Bundle
     * @return count of recipient
     */
    private static int sendMessageToClients(int arg1, int arg2, Bundle data) {
        int count = mClients.size();
        int sent = 0;
        for (int i = count - 1; i >= 0; i--) {
            try {
                // Send data
                Message m = Message.obtain(null, MSG_COMMAND, arg1, arg2, null);
                m.setData(data);
                mClients.get(i).send(m);
                sent++;
            } catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }
        return sent;
    }

    /**
     * Insert a new message to the content provider
     * @param list messages
     * @return count of updated/inserted records
     */
    private int addMessages(List<ToxMessage> list) {
        Context context = ToxClientService.getContext();
        if ((list == null) || (context == null))
            return 0;
        ContentResolver cr = context.getContentResolver();
        for (int i = 0; i < list.size(); i++) {
            ToxMessage m = list.get(i);
            ContentValues cv = ToxMessage.toContentValues(m);
            cr.insert(MessageProvider.CONTENT_URI, cv);
        }
        return list.size();
    }

    /**
     * Insert a new message to the content provider
     * @param value message
     */
    private static void addMessageToContentProvider(ToxMessage value) {
        Context context = ToxClientService.getContext();
        if ((value == null) || (context == null))
            return;
        ContentResolver cr = context.getContentResolver();
        ContentValues cv = ToxMessage.toContentValues(value);
        cr.insert(MessageProvider.CONTENT_URI, cv);
    }

    private void startTTS() {
        mTTS = new TextToSpeech(this, this);
    }

    @Override
    public void onInit(int status) {
        ApplicationSettings settings = ApplicationSettings.getInstance(this);
        if (settings == null)
            return;
        if ((status == TextToSpeech.SUCCESS) && (mTTS != null)) {
            settings.setTTSRun(this, true);
            Log.i(TAG, "TTS started successfully.");
        } else {
            settings.setTTSRun(this, false);
            mTTS = null;
            Log.e(TAG, "TTS is not started. Status = " + Integer.toString(status));
        }
    }

    /**
     * Say phrase
     * @param value phrase to say
     * @param language language identifier e.g. "en"
     * @return true if TTS ie enabled
     */
    public boolean say(String value, String language) {
        ApplicationSettings settings = ApplicationSettings.getInstance(this);
        if (settings == null)
            return false;

        if ((settings.isTtsRunning()) && (mTTS != null)) {
            Locale loc = new Locale(language);
            mTTS.setLanguage(loc);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Bundle alarm = new Bundle();
                alarm.putInt(TextToSpeech.Engine.KEY_PARAM_STREAM, AudioManager.STREAM_ALARM);
                mTTS.speak(value, TextToSpeech.QUEUE_ADD, alarm, TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID);
            } else {
                HashMap<String, String> alarm = new HashMap<>();
                alarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_ALARM));
                mTTS.speak(value, TextToSpeech.QUEUE_ADD, alarm);
            }
            return true;
        }
        if (settings.isTtsEnabled()) {
            // missing data, install it
            Helper.installTTS(this);
        }
        return false;
    }

    @Override
    public void onId(String id) {
        Log.i(TAG, "onId: " + id);
    }

    @Override
    public void onConnectionStatus(int status, String description) {
        Bundle b = new Bundle();
        if (mThreadClient != null && mThreadClient.isAlive() && mToxClientRunner != null) {
            b.putParcelable(KEY_CLIENT, mToxClientRunner.getToxClient());
        }
        b.putInt(KEY_NUMBER, status);
        b.putString(KEY_VALUE, description);
        sendMessageToClients(CMD_STATUS, 0, b);
        Log.i(TAG, "onConnectionStatus: " + Integer.toString(status) + " " + description);
    }

    @Override
    public void onMessage(int messageType, int friendNumber, String friendName, String body) {
        Bundle b = new Bundle();
        if (mThreadClient != null && mThreadClient.isAlive() && mToxClientRunner != null) {
            b.putParcelable(KEY_CLIENT, mToxClientRunner.getToxClient());
        }
        b.putInt(KEY_NUMBER, friendNumber);
        b.putInt(KEY_TYPE, messageType);
        b.putString(KEY_CN, friendName);
        String friendId = mToxClientRunner.getToxClient().getFriendId(friendNumber);
        b.putString(KEY_ID, friendId);
        b.putString(KEY_VALUE, body);

        Log.i(TAG, "onMessage: friend #" + Integer.toString(friendNumber) + ", id: "
                + friendId + ", nick: " + friendName + ", text: " + body);
        int sent = (int) (new Date().getTime() / 1000L);
        ToxMessage m = new ToxMessage(friendNumber, MessageProvider.OP_RECV, sent, friendId, friendName, messageType, body, 0);
        addMessageToContentProvider(m);
        mToxClientRunner.getToxClient().putMessage(messageType, friendNumber,body);
        int c = sendMessageToClients(CMD_MESSAGE, friendNumber, b);
        if (c <= 0) {
            // show notification
            Helper.showNotificationMessage(this, m);
        }
    }

    @Override
    public void onFriendRequest(String keyHex, String friendName) {
        Log.i(TAG, "onFriendRequest: " + keyHex + " " + friendName);
        Bundle b = new Bundle();
        if (mThreadClient != null && mThreadClient.isAlive() && mToxClientRunner != null) {
            b.putParcelable(KEY_CLIENT, mToxClientRunner.getToxClient());
        }
        b.putString(KEY_ID, keyHex);
        b.putString(KEY_CN, friendName);
        sendMessageToClients(CMD_FRIEND_REQUEST, 0, b);
    }
}