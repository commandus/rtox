package com.commandus.rtox;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * RTox application settings singleton
 */
public class ApplicationSettings {
	static final String PREFS_NAME = "rtox";
    private static final String PREF_USER_ID = "id";
	private static final String PREF_USER_CN = "cn";
	private static final String PREF_FIRST_TIME = "firsttime";
	private static final String PREF_TTS_ON = "ttson";
	private static final String PREF_STATUS_ONLINE = "status_online";
	private static final String PREF_STATUS_AWAY = "status_away";
	private static final String PREF_STATUS_BUSY = "status_busy";
	private static final String PREF_START = "start";

    private boolean mTTSRunning;
	private boolean mTTSEnabled;

	private static ApplicationSettings mInstance = null;
	private static boolean mFirstTime;
    private String mUserCN;
	private boolean mNoFriendRequest;
	private String mFriendRequestMessage;
	private String mOnlineStatus;
	private String mAwayStatus;
	private String mBusyStatus;
	private boolean mStart;

	public String getUserId() {
        return mUserId;
    }

    private String mUserId;

    synchronized static ApplicationSettings getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new ApplicationSettings(context);
		}
		return mInstance;
	}

	private  ApplicationSettings(Context context) {
		load(context);
	}

	String getUserCN() {
		return mUserCN;
	}
	static String getToxFileName(Context context) {
    	return context.getFilesDir() + "/rtox.tox";
	}

	void save(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREF_USER_ID, mUserId);
		editor.putString(PREF_USER_CN, mUserCN);
		editor.putBoolean(PREF_FIRST_TIME, mUserCN.isEmpty());
		editor.putBoolean(PREF_TTS_ON, true);
		editor.putString(PREF_STATUS_ONLINE, mOnlineStatus);
		editor.putString(PREF_STATUS_AWAY, mAwayStatus);
		editor.putString(PREF_STATUS_BUSY, mBusyStatus);
		editor.putBoolean(PREF_START, mStart);

		editor.apply();
		BackupAgent.requestBackup(context);
	}

    private void load(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        mUserId = settings.getString(PREF_USER_ID, "");
		mUserCN = settings.getString(PREF_USER_CN, "");
		mFirstTime = settings.getBoolean(PREF_FIRST_TIME, true);
		mTTSEnabled = settings.getBoolean(PREF_TTS_ON, true);
		mOnlineStatus = settings.getString(PREF_STATUS_ONLINE, "Online");
		mAwayStatus = settings.getString(PREF_STATUS_AWAY, "Away");
		mBusyStatus = settings.getString(PREF_STATUS_BUSY, "Busy");
		mStart = settings.getBoolean(PREF_START, true);
	}

	public boolean isFirstTime() {
		return mFirstTime;
	}

	/**
	 * Check is TTS enabled
	 * @return true
     */
	public boolean isTtsRunning() {
		return mTTSRunning;
	}

	public void setTTSRun(Context context, boolean on) {
		mTTSRunning = on;
        save(context);
	}

	public void enableTTS(Context context, boolean on) {
		mTTSEnabled = on;
		save(context);
	}

	public boolean isTtsEnabled() {
		return mTTSEnabled;
	}

	public boolean isStart() {
		return mStart;
	}

	public void setStart(boolean value) {
		mStart = value;
	}

    void clearUser(Context context) {
		mUserCN = "";
        mFirstTime = true;
		save(context);
	}

    public void setUserId(Context context, String value) {
        this.mUserId = value;
        save(context);
    }

    public void setUserCN(Context context, String value) {
        this.mUserCN = value;
        mFirstTime = false;
        save(context);
    }

	public boolean getNoFriendRequest() {
		return mNoFriendRequest;
	}

	public String getFriendRequestMessage() {
		return mFriendRequestMessage;
	}

    public String getOnlineStatus() {
        return mOnlineStatus;
    }

    public String getAwayStatus() {
		return mAwayStatus;
	}

	public String getBusyStatus() {
		return mBusyStatus;
	}

	public void setOnlineStatus(String value) {
		mOnlineStatus = value;
	}

	public void setAwayStatus(String value) {
		mAwayStatus = value;
	}

	public void setBusyStatus(String value) {
		mBusyStatus = value;
	}
}
