package com.commandus.rtox;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.commandus.data.Helper;
import com.commandus.provider.MessageAdapter;
import com.commandus.provider.MessageProvider;

import java.lang.ref.WeakReference;

/**
 * Main activity send Tox request messages to {@link ToxClientService}.
 */
public class MainActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor>{
    private static final String TAG = MainActivity.class.getSimpleName();
    private ApplicationSettings mAppSettings;

    private static Messenger mMessengerService = null;
    protected final Messenger mMessenger = new Messenger(new IncomingHandler(this));
    private boolean mServiceIsReady;

    private CursorAdapter mAdapter;

    private TextView mTextViewStatus;
    private ListView mListViewChatMsg;
    private String mConstraint;
    private EditText etNewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAppSettings = ApplicationSettings.getInstance(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        etNewMessage =  (EditText) findViewById(R.id.et_new_message);
        ImageButton bSendNewMessage =  (ImageButton) findViewById(R.id.b_send_new_message);
        bSendNewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTextMessageToFriend(0, etNewMessage.getText().toString());
            }
        });

        mTextViewStatus = (TextView) findViewById(R.id.tv_status);
        mListViewChatMsg = (ListView) findViewById(R.id.lv_messages);

        runService(mAppSettings.isStart());

        createAdapter();
    }

    private void sendTextMessageToFriend(int friendNumber, String message) {
        Bundle data = new Bundle();
        data.putInt(ToxClientService.KEY_TYPE, 0);    // 0- text message, 1- action
        data.putInt(ToxClientService.KEY_NUMBER, friendNumber);
        data.putString(ToxClientService.KEY_VALUE, message);
        Helper.sendMessage2Service(mMessengerService, mMessenger, ToxClientService.MSG_SEND_MESSAGE, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mAppSettings.isFirstTime())
        {
            Intent intent = new Intent(MainActivity.this, UserEditActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        runService(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem mi = menu.findItem(R.id.action_start_stop);
        if (mi != null) {
            if (mAppSettings.isStart())
                mi.setTitle(R.string.action_stop);
            else
                mi.setTitle(R.string.action_start);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                String id = ToxClientService.getToxId();
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, id);
                shareIntent.setType("text/plain");
                startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.action_share)));
                return true;
            case R.id.action_settings:
                return true;
            case R.id.action_start_stop:
                mAppSettings.setStart(!mAppSettings.isStart());
                runService(mAppSettings.isStart());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void runService(boolean value) {
        if (value) {
            bindService(new Intent(this, ToxClientService.class), mConnection, Context.BIND_AUTO_CREATE);
            startService(new Intent(this, ToxClientService.class));
        }
        else
        {
            if (mMessengerService != null) {
                try {
                    Message msg = Message.obtain(null, ToxClientService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mMessengerService.send(msg);
                }
                catch (RemoteException e) {
                    Log.e(TAG, e.toString());
                }
                unbindService(mConnection);
                mMessengerService = null;
            }
            if (mTextViewStatus != null) {
                mTextViewStatus.setText(R.string.msg_state_none);
            }

        }
        invalidateOptionsMenu();
    }

    /**
     * @see #processResponse(int, int, Bundle)
     */
    static class IncomingHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;

        IncomingHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = mActivity.get();
            switch (msg.what) {
                case ToxClientService.MSG_COMMAND:
                    // TODO get result
                    Bundle d = msg.getData();
                    if (d == null)
                        return;
                    activity.processResponse(msg.arg1, msg.arg2, d);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Process service response
     * @param arg1 arg 1
     * @param arg2 arg 2
     * @param data bundle
     * @see IncomingHandler#handleMessage(Message)
     */
    public void processResponse(int arg1, int arg2, Bundle data) {
        Context context = ToxClientService.getContext();
        switch (arg1) {
            case ToxClientService.CMD_STATUS:
                ToxClient c = data.getParcelable(ToxClientService.KEY_CLIENT);
                if (c == null) {
                    break;
                }
                int n = data.getInt(ToxClientService.KEY_NUMBER);
                String v = data.getString(ToxClientService.KEY_VALUE);
                if (mTextViewStatus != null) {
                    mTextViewStatus.setText(String.format(getString(R.string.msg_fmt_state), c.getId(), n, v));
                }
                break;
            case ToxClientService.CMD_FRIEND_REQUEST:
                String id = data.getString(ToxClientService.KEY_ID);
                String cn = data.getString(ToxClientService.KEY_CN);
                if ( mTextViewStatus != null) {
                    mTextViewStatus.setText(String.format(getString(R.string.msg_fmt_friend_request), id, cn));
                }
                break;
            case ToxClientService.CMD_MESSAGE:
                int friendNumber = data.getInt(ToxClientService.KEY_NUMBER);
                String friendId = data.getString(ToxClientService.KEY_ID);
                String friendName = data.getString(ToxClientService.KEY_CN);
                String body = data.getString(ToxClientService.KEY_VALUE);
                if ( mTextViewStatus != null) {
                    mTextViewStatus.setText(String.format(getString(R.string.msg_fmt_message),
                            friendId, friendName, body));
                }
                break;
            default:
                break;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mMessengerService = new Messenger(service);
            try {
                Message msg = Message.obtain(null, ToxClientService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mMessengerService.send(msg);
                mServiceIsReady = true;
                String id = ToxClientService.getToxId();
                mTextViewStatus.setText(id);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
                Log.e(TAG, "Service connect error " + e.toString());
            }
            startSession();
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            mMessengerService = null;
            finishSession();
        }
    };

    private void startSession() {
        if (!mServiceIsReady)
            return;
        Bundle data = new Bundle();
        Helper.sendMessage2Service(mMessengerService, mMessenger, ToxClientService.MSG_START_SESSION, data);
    }

    private void finishSession() {
        if (!mServiceIsReady)
            return;
        Bundle data = new Bundle();
        Helper.sendMessage2Service(mMessengerService, mMessenger, ToxClientService.MSG_FINISH_SESSION, data);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mConstraint != null)
            return new CursorLoader(this,
                MessageProvider.CONTENT_URI, null, MessageProvider.F_BODY + " LIKE \"%?%\"",
                new String[] {mConstraint}, null);
        else
            return new CursorLoader(this,
                    MessageProvider.CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    private void createAdapter() {
        if (mAdapter != null) {
            getLoaderManager().destroyLoader(0);
        }

        mAdapter = new MessageAdapter(this, android.R.layout.simple_list_item_1, null,
                new String[] {MessageProvider.F_BODY}, new int[] {android.R.id.text1}, 0);
        if (mListViewChatMsg != null) {
            mListViewChatMsg.setAdapter(mAdapter);
        }

        getSupportLoaderManager().initLoader(0, null, this);
    }

}
