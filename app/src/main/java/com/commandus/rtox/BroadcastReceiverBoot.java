package com.commandus.rtox;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Receive android.intent.action.BOOT_COMPLETED broadcast and start up
 * {@link ToxClientService} which is responsible to the web service communication.
 */
public class BroadcastReceiverBoot extends BroadcastReceiver {
    private static final String TAG = BroadcastReceiverBoot.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "Starting service on boot");
		context.startService(new Intent(context, ToxClientService.class));
    }
}
