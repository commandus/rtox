package com.commandus.rtox;

import android.os.Parcel;
import android.os.Parcelable;

import com.commandus.data.BootstrapNode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ToxClient implements Parcelable {
    static {
        System.loadLibrary("c-toxcore");
    }
    private long mClient;

    private native long createClient(
            ToxReceiverInterface receiver,
            Object nodes[],
            String filename,
            String nick,
            String status,
            boolean no_friend_request,
            String friend_request_message,
            String online_status,
            String away_status,
            String busy_status
    );
    private native long destroyClient(
            long client
    );

    private native String getId(
            long client
    );

    private native String getFriendId(
            long client,
            int friendNumber
    );

    private native String getFriendName(
            long client,
            int friendNumber
    );

    private native int getFriendSize(
            long client
    );

    private native int run(
            long client
    );

    private native void putMessage(
            long client,
            int messageType,
            int friendNumber,
            String message
    );

    public ToxClient(
            ToxReceiverInterface receiver,
            List<BootstrapNode> nodes,
            String filename,
            String nick,
            String status,
            boolean no_friend_request,
            String friend_request_message,
            String online_status,
            String away_status,
            String busy_status
    )
    {
        mClient = createClient(receiver, nodes.toArray(), filename, nick, status, no_friend_request,
                friend_request_message, online_status, away_status, busy_status);
    }

    public void close()
    {
        destroyClient(mClient);
        mClient = 0;
    }

    public String getId()
    {
        return getId(mClient);
    }


    public String getFriendId(
            int friendNumber
    )
    {
        return getFriendId(mClient, friendNumber);
    }

    public String getFriendName(
            int friendNumber
    )
    {
        return getFriendName(mClient, friendNumber);
    }

    public int run()
    {
        return run(mClient);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mClient);
    }

    public static final Parcelable.Creator<ToxClient> CREATOR = new Parcelable.Creator<ToxClient>() {
        public ToxClient createFromParcel(Parcel in) {
            return new ToxClient(in);
        }

        public ToxClient[] newArray(int size) {
            return new ToxClient[size];
        }
    };

    private ToxClient(Parcel parcel) {
        mClient = parcel.readLong();
    }

    public Map<String, String> getFriends() {
        HashMap<String, String> r = new HashMap<>();
        int size = getFriendSize(mClient);
        for (int i = 0; i < size; i++) {
            String key = getFriendId(mClient, i);
            String cn = getFriendName(mClient, i);
            r.put(key, cn);
        }
        return r;
    }

    public void putMessage(
        int messageType,
        int friendNumber,
        String message
    )
    {
        putMessage(mClient, messageType, friendNumber, message);
    }

}
