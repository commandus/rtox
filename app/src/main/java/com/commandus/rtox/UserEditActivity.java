package com.commandus.rtox;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class UserEditActivity extends AppCompatActivity
{

    private static final String TAG = UserEditActivity.class.getSimpleName();
    private EditText mCN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);

        ActionBar b = getSupportActionBar();
        if (b != null)
            b.setDisplayHomeAsUpEnabled(true);

        mCN = (EditText) findViewById(R.id.et_user_cn);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void mkUser(String cn) {
        ApplicationSettings s = ApplicationSettings.getInstance(this);
        s.setUserCN(this, cn);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_edit, menu);
        MenuItem mi = menu.findItem(R.id.action_adduser);
        if (mi != null)
            mi.setEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_adduser) {
            String cn = mCN.getText().toString().trim();
            if (cn.isEmpty()) {
                Toast.makeText(this, R.string.error_empty_cn, Toast.LENGTH_LONG).show();
                return true;
            }
            mkUser(cn);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
