package com.commandus.rtox;

import android.app.backup.BackupAgentHelper;
import android.app.backup.BackupManager;
import android.app.backup.FileBackupHelper;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;

public class BackupAgent extends BackupAgentHelper {
    @Override
    public void onCreate() {
        FileBackupHelper fileBackupHelper = new FileBackupHelper(this, getFilesDir()+ "/rtox.tox");
        addHelper("files", fileBackupHelper);
        SharedPreferencesBackupHelper helper = new SharedPreferencesBackupHelper(this, ApplicationSettings.PREFS_NAME);
        addHelper("prefs", helper);
    }

    // Call on data changed
    public static void requestBackup(Context context) {
        new BackupManager(context).dataChanged();
    }
}
