package com.commandus.rtox;

public interface ToxReceiverInterface {
    void onId(String id);
    void onConnectionStatus(int status, String description);
    void onMessage(int messageType, int friendNumber, String friendName, String message);
    void onFriendRequest(String keyHex, String friendName);
}
