package com.commandus.tox;

import android.util.Log;

import com.commandus.data.BootstrapNode;
import com.commandus.rtox.ToxClient;
import com.commandus.rtox.ToxReceiverInterface;

import java.util.List;

public class ToxClientRunner implements Runnable, ToxReceiverInterface {
    private static final String TAG = ToxClientRunner.class.getSimpleName();

    private ToxClient mToxClient;
    private ToxReceiverInterface mParent;
    private String filename;
    private String nick;
    private String status;
    private List<BootstrapNode> mNodes;
    boolean mNoFriendRequest;
    String friendRequestMessage;
    String onlineStatus;
    String awayStatus;
    String busyStatus;

    public ToxClient getToxClient() {
        return mToxClient;
    }

    public ToxClientRunner(ToxReceiverInterface mParent, List<BootstrapNode> nodes, String filename,
           String nick, String status, boolean no_friend_request,
           String friend_request_message, String onlineStatus, String awayStatus, String busyStatus
    ) {
        mNodes = nodes;
        this.mParent = mParent;
        this.filename = filename;
        this.nick = nick;
        this.status = status;
        this.mNoFriendRequest = no_friend_request;
        this.friendRequestMessage = friend_request_message;
        this.onlineStatus = onlineStatus;
        this.awayStatus = awayStatus;
        this.busyStatus = busyStatus;
    }

    @Override
    public void onId(String id) {
    }

    @Override
    public void onConnectionStatus(int status, String description) {
        Log.i(TAG, "onConnectionStatus: " + Integer.toString(status) + " " + description);
        mParent.onConnectionStatus(status, description);
    }

    @Override
    public void onMessage(int message_type, int friendnumber, String friendName, String message) {
        mParent.onMessage(message_type, friendnumber, friendName, message);
    }

    @Override
    public void onFriendRequest(String keyHex, String friendName) {
        mParent.onFriendRequest(keyHex, friendName);
    }

    @Override
    public void run() {
        // updateBoostrapNodes();
        clientLoop(mNodes);
    }

    private void clientLoop(List<BootstrapNode> nodes) {
        try {
            mToxClient = new ToxClient(this, nodes, filename, nick, status, mNoFriendRequest,
                    friendRequestMessage, onlineStatus, awayStatus, busyStatus
            );
            Log.i(TAG, "Tox client running..");
            mToxClient.run();
            mToxClient.close();
            Log.i(TAG, "Tox client stopped..");
            mToxClient = null;
        } catch (Exception e) {
            Log.i(TAG, "Tox client interrupted.");
        }
    }
}
