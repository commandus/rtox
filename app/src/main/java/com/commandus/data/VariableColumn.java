package com.commandus.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Variable at column of IntentTemplate
 * @see IntentTemplateHelper
 */
public class VariableColumn implements Parcelable {
	private static final char SEPARATOR_2 = ':';
	private Variable variable;
	private int column;
	private int idx;
	private String name;
	private int len;
	private boolean encode;

    public VariableColumn(Variable variable, int column, int idx, String name, int len, boolean encode) {
        this.variable = variable;
        this.column = column;
        this.idx = idx;
        this.name = name;
        this.len = len;
        this.encode = encode;
    }

	public VariableColumn(Parcel parcel) {
		this.variable = Variable.toVariable(parcel.readInt());
		this.column = parcel.readInt();
		this.idx = parcel.readInt();
		this.name = parcel.readString();
		this.len = parcel.readInt();
		boolean[] val = new boolean[1];
		parcel.readBooleanArray(val);
		this.encode = val[0];
	}

	public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public boolean isEncode() {
        return encode;
    }

    public void setEncode(boolean encode) {
        this.encode = encode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(variable.toInteger());
        dest.writeInt(column);
        dest.writeInt(idx);
        dest.writeString(name);
        dest.writeInt(len);
        dest.writeBooleanArray(new boolean[] {encode});
    }

	public static final Parcelable.Creator<VariableColumn> CREATOR = new Parcelable.Creator<VariableColumn>() {
		public VariableColumn createFromParcel(Parcel parcel) {
			return new VariableColumn(parcel);
		}

		@Override
		public VariableColumn[] newArray(int size) {
			return new VariableColumn[size];
		}
	};

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		VariableColumn that = (VariableColumn) o;

		if (column != that.column) return false;
		if (idx != that.idx) return false;
		if (len != that.len) return false;
		if (encode != that.encode) return false;
		if (variable != that.variable) return false;
		return name != null ? name.equals(that.name) : that.name == null;
	}

	@Override
	public int hashCode() {
		int result = variable != null ? variable.hashCode() : 0;
		result = 31 * result + column;
		result = 31 * result + idx;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + len;
		result = 31 * result + (encode ? 1 : 0);
		return result;
	}

	public enum Variable {
        NONE,
        PHONE,
        IMEI,
        NETWORKID,
        ID,
        NAME,
        NOW,
        LATITUDE,
        LONGITUDE,
        CID,
        OSNAME,
        OSVER,
        MODEL,
        URL_PHOTO,
        URL_AUDIO,
        URL_VIDEO,
        SEL_PHONE,
        SEL_EMAIL,
        SEL_APP,
        INPUT_PHONE,
        INPUT_EMAIL,
        INPUT_HTTP,
        INPUT_NUMBER,
        INPUT_DATE,
        INPUT_TIME,
        INPUT_LINE,
        INPUT_TEXT;

        private static final String TAG = VariableColumn.class.getSimpleName();
        private static Variable[] allValues = values();

        public static Variable toVariable(int value) {
            try
            {
                return allValues[value];
            }
            catch (Exception e)
            {
                Log.e(TAG, e.toString());
            }
            return NONE;
        }

		// TODO it is not recommended
        public int toInteger() {
            return this.ordinal();
        }
	}

	/**
	 * Return string ready to parse with {@link IntentTemplateHelper#parse(String)}
	 * Called from {@link IntentTemplate#toString()}
	 * Separators: '|'- intent template attrs, ','- VariableColumn list separator, ':'- VariableColumn attribute separator
	 * where 0- lang 1-image uri 2- name, 3- description, 4- template string, 5..- "," separated column:variable:name:len:e index pairs array
	 * lang|img|name|desc|template|column:variable:name:len:e,... e.g. 0:15:0:0:e,4:16:0:0:e
	 * Special intent extra e.g.
	 *      S.com.commandus.intent.fix.array=android.intent.extra.EMAIL,android.intent.extra.SUBS
	 * @return String
	 * @see IntentTemplate#toString()
	 */
	@Override
	public String toString() {
		return new StringBuilder()
			.append(getColumn()).append(SEPARATOR_2)
				.append(getVariable().toInteger()).append(SEPARATOR_2)
				.append(getName()).append(SEPARATOR_2)
				.append(getLen()).append(SEPARATOR_2)
				.append(isEncode() ? "e" : "").toString();
	}

}
