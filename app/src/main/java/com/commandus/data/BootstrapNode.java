package com.commandus.data;

public class BootstrapNode {
    public String ip;
    public int port;
    public String publicKey;

    public BootstrapNode(String ip, int port, String publicKey) {
        this.ip = ip;
        this.port = port;
        this.publicKey = publicKey;
    }
}
