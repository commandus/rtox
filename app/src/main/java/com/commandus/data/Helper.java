package com.commandus.data;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ListView;

import com.commandus.rtox.MainActivity;
import com.commandus.rtox.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

/**
 * Helper class provides static methods to install app, get value line #1 number, improved version of parseUri etc.
 */
public class Helper {

    private static final String FixTypeSuffix = "com.commandus.intent.fix.array";
    private static final String TAG = Helper.class.getSimpleName();
    private static int icon_stat_message = R.drawable.ic_stat_notify_message;

    // private static final String[] FixTypePrefix = {"S", "B", "b", "c", "d", "f", "i", "l", "s"};

	/**
     * Send message to binded service (messengerTo) and wait response on messengerReply
     * @param messengerTo   destination messenger
     * @param messengerReply    receiver messenger
     * @param msg   message to send
     * @param data  message data
     */
    public static void sendMessage2Service(Messenger messengerTo, Messenger messengerReply, int msg, Bundle data) {
        Message message = Message.obtain(null, msg);
        if (data != null)
            message.setData(data);
        message.replyTo = messengerReply;
        try {
            messengerTo.send(message);
        } catch (RemoteException e) {
            Log.e(TAG, e.toString());
        }
    }

    /**
     * Parse intent string and return Intent object, including parameters which must passed
     * as arrays.
     * <p>
     * Important notice:
     * <p>
     * Android's intent parser do not parse arrays. Some intents however include arrays.
     * Intent must provide special extra parameter
     * 	S.com.commandus.intent.fix.array=EXTRA_KEY_1,EXTRA_KEY_2,...,
     * where EXTRA_KEY_1,EXTRA_KEY_2,... - keys which have array type and it must be fixed.
     * <p>
     * Last S.com.commandus.intent.fix.array extra then deleted from the intent.
     * <p>
     * Note: You can pass however just one parameter in array (may be I'll fix it later).
     * @param uri Uri to parse
     * @return Intent or null on fail
     */
    public static Intent parseUri(String uri) {
        Intent r = null;
        try {
            r = Intent.parseUri(uri, 0);
            Bundle extras = r.getExtras();
            if (extras != null) {
                // find out special extra indicate key of array
                String key = FixTypeSuffix;
                Object o = extras.get(key);
                if (o != null) {
                    String arrs = o.toString();
                    if (arrs != null) {
                        String arr[] = arrs.split("\\,");
                        for (String k : arr) {
                            Object ob = extras.get(k);
                            if (ob != null) {
                                if (ob instanceof String) {
                                    String vS = extras.getString(k);
                                    r.removeExtra(k);
                                    r.putExtra(k, new String[] {vS});
                                }
                                // TODO implement other types
                            }
                        }
                    }
                    // remove
                    r.removeExtra(key);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return r;
    }

	/**
     * Invoke Play Market "Install" activity to run activity in the provided intent
     * @param context Application context
     * @param intent Explicit intent (contains application package)
     */
    public static void installApp(Context context, Intent intent) {
        if (intent == null)
            return;
        String name = null;
        boolean isPackage = false;
        ComponentName component = intent.getComponent();
        if (component != null) {
            name = component.getPackageName();
            isPackage = (name != null && !name.isEmpty());// name != null;
        }
        if (!isPackage)
            name = intent.getAction();
        if (name == null)
            name = "";
        installApp(context, name, isPackage);
    }

	/**
	 * Invoke Play Market "Install" application package
	 * @param context Context
	 * @param name package name
	 * @param isPackageName true- is package
	 */
    public static void installApp(
            final Context context,
            final String name,
            final boolean isPackageName
    ) {
        Resources rs = context.getResources();

        Dialog dialog = new AlertDialog.Builder(context)
                .setMessage(R.string.dlg_install_message)
                .setTitle(R.string.dlg_install_title)
                .setPositiveButton(rs.getString(R.string.dlg_vr_positive), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            String prefix;
                            if (isPackageName)
                                prefix = "market://details?id=";
                            else
                                prefix = "market://search?q=";
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(prefix  + name));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
                            } else {
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                            }
                            context.startActivity(intent);
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }})
                .setNegativeButton(rs.getString(R.string.dlg_vr_negative), null)
                .create();
        dialog.show();
    }

    /**
     * Like {@link android.os.Build.VERSION#SDK_INT}, but in a place where it can be conveniently
     * overridden for local testing.
     */
    public static final int SDK_INT =
            (Build.VERSION.SDK_INT == 23 && Build.VERSION.CODENAME.charAt(0) == 'N') ? 24
                    : Build.VERSION.SDK_INT;

	/**
     * Parse value number
     * @param value value number e.g. "+1 801 880 0903"
     * @return long number. -1 if value string is null, 0 if number is invalid
     */
    public static long phoneNumber(String value) {
        if (value == null)
            return -1;
        try {
            return Long.parseLong(value.replaceAll("\\D+", ""));
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Format phone number
     * @param context Context
     * @param value phone number
     * @return E.164 formatted phone number
     */
    public static String phoneToE164(Context context, String value) {
        if (value == null)
            return "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String countryCode = Locale.getDefault().getCountry();
			String r = PhoneNumberUtils.formatNumberToE164(value, countryCode);
			if (r == null)
				return value;
			else
            	return r;
        }
        else {
            if (value.length() >= 11)
                return "+" + value; // TODO improve!
            else
                return value;
        }
    }

	/**
     * Format phone number
     * @param context Context
     * @param value phone number
     * @return E.164 formatted phone number
     */
    public static String phoneToE164(Context context, long value) {
        return phoneToE164(context, String.valueOf(value));
    }

    /**
     * Helper static method to read data from the application resource.
     * like Internet service or Android data provider.
     * @param context	Context
     * @param id	application resource id
     * @return	byte array. null if fail
     */
    public static byte[] loadResource(Context context, int id) {
        InputStream strm;
        try {
            strm = context.getResources().openRawResource(id);
        } catch (Exception e) {
            return null;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        copy(baos, strm);
        byte[] r = baos.toByteArray();
        try {
            strm.close();
            baos.close();
        } catch (IOException e) {
            Log.e(TAG, e.toString());
        }
        return r;
    }

    /**
     * Copy stream from src to dest
     * @param dest	destination stream
     * @param src	source stream
     * @return	true if success
     */
    private static boolean copy(OutputStream dest, InputStream src) {
        int b;
        byte[] buffer = new byte[1024];
        do {
            try {
                b = src.read(buffer);
                if (b <= 0)
                    break;
                dest.write(buffer, 0, b);
            } catch (IOException e) {
                return false;
            }
        } while (true);
        return true;
    }

    /**
     * Get resource Uri string
     * @param context   Context
     * @param resourceId    resource id
     * @return String
	 */
    public static String getResourceUri(Context context, int resourceId) {
        return "android.resource://" + context.getPackageName() + "/" + resourceId;
    }

	/**
     * listview get item view by position
     * @param position   position
     * @param listView listview
     * @return list item
     *
     * @see "http://stackoverflow.com/questions/24811536/android-listview-get-item-view-by-position"
     */
    public static View getViewByPosition(ListView listView, int position) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (position < firstListItemPosition || position > lastListItemPosition ) {
            return listView.getAdapter().getView(position, null, listView);
        } else {
            final int childIndex = position - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    private static float[] animRL = { 0.0f, -1.0f, 0.0f, 0.0f };
    private static float[] animLR = { 0.0f, +1.0f, 0.0f, 0.0f };

    private static Animation outToAnimation(final Activity activity, final View view, int duration, float[] animArray) {
        final Animation anim = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, animArray[0],
                Animation.RELATIVE_TO_PARENT, animArray[1],
                Animation.RELATIVE_TO_PARENT, animArray[2],
                Animation.RELATIVE_TO_PARENT, animArray[3]);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        view.setHasTransientState(true);	// http://stackoverflow.com/questions/12595610/animate-the-removal-of-a-listview-item
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setHasTransientState(false);
                /*
                ViewGroup parent = (ViewGroup) view.getParent();
                int index = parent.indexOfChild(view);
                parent.removeView(view);
                View v = activity.getLayoutInflater().inflate(R.layout.listitem_undelete, parent, false);
                parent.addView(v, index);
                */
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return anim;
    }

    public static Animation outToLeftAnimation(Activity activity, View view, int duration) {
        return outToAnimation(activity, view, duration, animRL);
    }

    public static Animation outToRightAnimation(Activity activity, View view, int duration) {
        return outToAnimation(activity, view, duration, animLR);
    }

	/**
	 * Show dialog to install TTS
	 * @param context	Application context
	 */
    public static void installTTS(Context context) {
        // missing data, install it
        Intent installIntent = new Intent();
        installIntent.setAction(
                TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
        context.startActivity(installIntent);
    }

    public static void showNotificationMessage(Context context, ToxMessage value) {
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0);
        Notification.Builder builder;
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            return;
        }
        String sFrom = String.format(context.getString(R.string.msg_fmt_message),
                value.getUserId(), value.getCN(), value.getBody());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String channelId = context.getString(R.string.notification_channel_message_id);
            NotificationChannel ch = new NotificationChannel(
                    channelId, context.getString(R.string.notification_channel_message_name), NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(ch);
            builder = new Notification.Builder(context, channelId)
                    .setContentTitle(sFrom)
                    .setContentText(value.getBody())
                    .setContentIntent(contentIntent)
                    .setSmallIcon(icon_stat_message)
                    .setAutoCancel(true);
        } else {
            builder = new Notification.Builder(context)
                    .setContentTitle(sFrom)
                    .setContentText(value.getBody())
                    .setContentIntent(contentIntent)
                    .setSmallIcon(icon_stat_message)
                    .setAutoCancel(true);
        }

        Notification notification;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        } else {
            notification = builder.getNotification();
        }

        notificationManager.notify(icon_stat_message, notification);
    }

}
