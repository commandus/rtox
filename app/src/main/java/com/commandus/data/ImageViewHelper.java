package com.commandus.data;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Helper class to manipulate ImageView
 */
public class ImageViewHelper {

	public static final String CONTENT_TYPE_PNG = "image/png";

	/**
	 * Return ImageView's bitmap as PNG byte array
	 *
	 * @param value ImageView
	 * @return PNG array
	 */
	public static byte[] ImageViewToPng(ImageView value) {
		if (value == null)
			return null;
		value.setDrawingCacheEnabled(true);
		value.buildDrawingCache();
		Bitmap bm = value.getDrawingCache();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
		return stream.toByteArray();
	}
}