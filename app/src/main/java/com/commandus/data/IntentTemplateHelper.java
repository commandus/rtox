package com.commandus.data;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.Log;
import android.util.SparseArray;

/**
 * IntentTemplate parser and builder
 */
public class IntentTemplateHelper {
    private static final String TAG = IntentTemplateHelper.class.getSimpleName();
    private IntentTemplate mIntentTemplate;
    private Context mContext;
    private Resources mResources;
    private Vector <VariableColumn> mVariableColumns;
    private SparseArray<Object> mVariableValues;

    @SuppressWarnings("unchecked")
    public IntentTemplateHelper(Context context, IntentTemplate intentTemplate) {
        super();
        mIntentTemplate = intentTemplate;
        mVariableColumns = mIntentTemplate.getVariableColumnList();
        mVariableValues = new SparseArray<>();
        mContext = context;
        mResources = mContext.getResources();
    }

    public IntentTemplate getIntentTemplate() {
        return mIntentTemplate;
    }

    public Object getVariableValue(int idx) {
        for (VariableColumn vc : mVariableColumns) {
            if (vc.getIdx() == idx) {
                return mVariableValues.get(idx) != null;
            }
        }
        return null;
    }

    /**
     * Set variable value by the index. Index is order in the form starting with 0.
     * @param idx	Index of variable 0.. is order how variable is shown to the user
     * @param value	value to set
     * @return	true if index exists
     */
    public boolean setVariableValueByIndex(int idx, Object value) {
        for (VariableColumn vc : mVariableColumns) {
            if (vc.getIdx() == idx) {
                mVariableValues.put(idx, value);
                return true;
            }
        }
        return false;
    }

    /**
     * Set variable value by the variable. Index is order in the form starting with 0.
     * @param v Variable to set
     * @param idx	<0 - all variables, >=0 variable at position idx to set
     * @param value	value to set
     * @return	true if index exists
     */
    public boolean setVariableValueByVariable(VariableColumn.Variable v, int idx,Object value) {
        int c = 0;
        boolean ok = false;
        for (VariableColumn vc : mVariableColumns) {
            if (vc.getVariable() == v) {
                if ((idx == c) || (idx < 0)) {
                    mVariableValues.put(vc.getIdx(), value);
                    ok = true;
                    if (idx == c)
                        break;
                    c++;
                }
            }
        }
        return ok;
    }

    public boolean isVariableValue(int idx) {
        for (VariableColumn vc : mVariableColumns) {
            if (vc.getIdx() == idx) {
                return mVariableValues.get(idx) != null;
            }
        }
        return false;
    }

    /**
     * Check variable values existence
     * @return -1 if all variable has value set, otherwise return first unfilled variable column
     */
    public int checkVariableValues() {
        int idx;
        for (VariableColumn vc : mVariableColumns) {
            idx = vc.getIdx();
            if (mVariableValues.get(idx) == null)
                return idx;
        }
        return -1;
    }

    /**
     * Fill template with variable values
     * @return Intent string from IntentTemplate with variable's values
     * @see #setVariableValueByIndex(int, Object)
     */
    public String buildIntent() {
        String template = mIntentTemplate.getTemplateString();
        String v;
        StringBuilder b = new StringBuilder();
        int start = 0;
        int end;
        int i = 0;
        for (VariableColumn vc : mVariableColumns) {
            end = vc.getColumn();
            b.append(template.substring(start, end));
            Object o = mVariableValues.get(i);
            if (o != null) {
                if (vc.isEncode())
                    try {
                        v = URLEncoder.encode(o.toString(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        v = "";
                    }
                else
                    v = o.toString();
            } else {
                v = "";
            }
            b.append(v); // ;)
            start = end;
            i++;
        }
        if (start < template.length())
            b.append(template.substring(start)); // ;)
        return b.toString();
    }

    /**
     * Return template as regular expression string
     * @return Intent string from IntentTemplate with variable's values
     * @see #setVariableValueByIndex(int, Object)
     */
    public String getREString() {
        String template = mIntentTemplate.getTemplateString();
        StringBuilder b = new StringBuilder();
        String t;
        int start = 0;
        int end;
        for (VariableColumn vc : mVariableColumns) {
            end = vc.getColumn();
            t = escapeRE(template.substring(start, end));
            b.append(t);
            b.append(getREvar(vc.getVariable()));
            start = end;
        }
        if (start < template.length()) {
            t = escapeRE(template.substring(start));
            b.append(t);
        }
        return b.toString();
    }

    /**
     * Escape regular expression special character
     * @param value string to escape regular expression
     * @return string
     */
    private String escapeRE(String value) {
        if (value == null)
            return "";
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            if (!Character.isLetterOrDigit(c))
                b.append('\\');
            b.append(c);
        }
        return b.toString();
    }

    /**
     * Return regular expression for variable
     * @param variable {@link VariableColumn.Variable}
     * @return just return .* :(
     */
    private String getREvar(VariableColumn.Variable variable) {
        return ".*";
    }

    /**
     * Return {@link VariableColumn} at the position
     * @param position
     * @return null if out of bound
     */
    public VariableColumn getVariableColumnByIdx(int position) {
        int sz = mVariableColumns==null?0:mVariableColumns.size();
        for (int i = 0; i < sz; i++) {
            VariableColumn vc = mVariableColumns.get(i);
            if ((vc != null) && (vc.getIdx() == position))
                return vc;
        }
        return null;
    }

    public String getResStringOfVarAtIndex(int idx, TypedArray resArray) {
        String r = "";
        VariableColumn vc = getVariableColumnByIdx(idx);
        if (vc != null)
            r = resArray.getString(vc.getVariable().toInteger() - 1);
        return r;
    }

    public boolean getResBooleanOfVarAtIndex(int idx, TypedArray resArray) {
        boolean r = false;
        VariableColumn vc = getVariableColumnByIdx(idx);
        if (vc != null)
            r = resArray.getBoolean(vc.getVariable().toInteger() - 1, false);
        return r;
    }

    public String getTitleName(int resourceId, int idx) {
        TypedArray names = mResources.obtainTypedArray(resourceId);
        String r = getResStringOfVarAtIndex(idx, names);
        names.recycle();
        return r;
    }

    public String getTitleHint(int resourceId, int idx) {
        TypedArray hints = mResources.obtainTypedArray(resourceId);
        String r = getResStringOfVarAtIndex(idx, hints);
        hints.recycle();
        return r;
    }

    public boolean getTitleVisible(int resourceId, int idx) {
        TypedArray visibles = mResources.obtainTypedArray(resourceId);
        boolean r = getResBooleanOfVarAtIndex(idx, visibles);
        visibles.recycle();
        return r;
    }

    /**
     * Parse intent template from the string in given format
     * @param value String like "Application|Start selected application|a=&b=|2:12:0:e,5:100:0:e)"
     * Separators: '|'- intent template attrs, ','- VariableColumn list separator, ':'- VariableColumn attribute separator
     * where 0- lang 1-image uri 2- name, 3- description, 4- template string, 5..- "," separated column:variable:name:len:e index pairs array
     * @return IntentTemplate
     */
    @SuppressWarnings("unchecked")
    public static IntentTemplate parse(String value) {
        IntentTemplate r = new IntentTemplate();

        String[] s = value.split("\\|");
        // TODO determine language from resource
        r.setLang(s[0]);
        // r.setImage(s[1]);
        r.setName(s[2]);
        r.setDescription(s[3]);
        r.setTemplateString(s[4]);
        String[] vcs = s[5].split("\\,");

        @SuppressWarnings("rawtypes")
        Vector vcl = new Vector <>();
        short idx = 0;
        for (String pair: vcs) {
            String[] p = pair.split("\\:");
            if (p.length >= 3) {
                short len = 0;
                if (p.length >= 4) {
                    try {
                        len = Short.parseShort(p[3]);
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage() + " at " + pair);
                        continue;
                    }
                    if (len < 0)
                        len = 0;
                }
                boolean encode = ((p.length >= 5) && (p[4].length() > 0) && (p[4].startsWith("e")));

                short col;
                try {
                    col = Short.parseShort(p[0]);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage()  + " at " + pair);
                    continue;
                }
                vcl.add(new VariableColumn(
                        VariableColumn.Variable.toVariable(Integer.parseInt(p[1])),
                        col,
                        idx, p[2], len, encode
                ));
                idx++;
            }
        }
        r.setVariableColumnList(vcl);
        return r;
    }

    /**
     * Try to find out template used to fill intent
     * @param intent Intent string
     * @return "" if not found
     */
    public static String getTemplateByIntent(Context context, List<IntentTemplate> its, String intent) {
        String r = "";
        if (its == null)
            return r;
        for (IntentTemplate t : its) {
            IntentTemplateHelper helper = new IntentTemplateHelper(context, t);
            String reTemplate = helper.getREString();
            if (intent.matches(reTemplate)) {
                String newTemplate = t.getTemplateString();
                if (r.length() < newTemplate.length())
                    r = newTemplate;
            }
        }
        return r;
    }

}
