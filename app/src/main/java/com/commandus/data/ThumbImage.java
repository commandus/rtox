package com.commandus.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.net.Uri;
import android.util.Log;

/**
 * Thumbnail image helper class make fixed size thumbs from image. 
 */
public class ThumbImage {
	private static final String TAG = "FileThumbImage";
	private static final int THUMBNAIL_WIDTH = 256;
	private static final int THUMBNAIL_HEIGHT = THUMBNAIL_WIDTH;
	// private static final int THUMBNAIL_WIDTH = 128;
	public static final String DIR = "th";
	public static final String FILENAME_PREFIX = "th";
	public static final String FILENAME_SUFFIX = ".png";
	public static final int THUMB_STYLE_WRAP = 0;
	public static final int THUMB_STYLE_ROUNDED = 2;

	/**
	 * @param path   path
	 * @param dir    can be null
	 * @param prefix file name prefix
	 * @param l      file number
	 * @return String
	 */
	public static String getFileName(String path, String dir, String prefix, long l) {
		return path + "/" + (dir == null ? "" : dir + "/") + FILENAME_PREFIX + prefix + "." + l + FILENAME_SUFFIX;
	}

	/**
	 * Make thumb bitmap for PNG data
	 *
	 * @param context Context
	 * @param uri     can be null
	 * @param style   {@link ThumbImage#THUMB_STYLE_WRAP}- wrap, {@link ThumbImage#THUMB_STYLE_ROUNDED}- center&rounded
	 * @return thumb bitmap for PNG data or null
	 * @see ThumbImage#makeThumb(Bitmap, int)
	 */
	public static Bitmap makeThumb(Context context, Uri uri, int style) {
		if (uri == null)
			return null;
		Bitmap imageBitmap;
		try {
			InputStream strm = context.getContentResolver().openInputStream(uri);
			imageBitmap = BitmapFactory.decodeStream(strm);
			if (strm != null) {
				strm.close();
			}
		} catch (Exception e) {
			// java.lang.OutOfMemoryError
			Log.e(TAG, "makeThumb(2) " + e.toString());
			return null;
		}
		Bitmap r = makeThumb(imageBitmap, style);
		if (imageBitmap != null)
			imageBitmap.recycle();
		return r;
	}


	/**
	 * Make thumb bitmap for PNG data
	 *
	 * @param data  can be null
	 * @param style {@link ThumbImage#THUMB_STYLE_WRAP}- wrap, {@link ThumbImage#THUMB_STYLE_ROUNDED}- center&rounded
	 * @return thumb bitmap for PNG data or null
	 * @see ThumbImage#makeThumb(Bitmap, int)
	 */
	private static Bitmap makeThumb(byte[] data, int style) {
		if (data == null)
			return null;
		Bitmap imageBitmap;
		try {
			imageBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		} catch (Exception e) {
			// java.lang.OutOfMemoryError
			Log.e(TAG, "makeThumb(2) " + e.toString());
			return null;
		}
		Bitmap r = makeThumb(imageBitmap, style);
		imageBitmap.recycle();
		return r;
	}

	/**
	 * Make thumb bitmap for PNG data
	 *
	 * @param src   can be null
	 * @param style {@link ThumbImage#THUMB_STYLE_WRAP}- wrap, {@link ThumbImage#THUMB_STYLE_ROUNDED}- center&rounded
	 * @return thumb bitmap for PNG data or null
	 */
	public static Bitmap makeThumb(Bitmap src, int style) {
		if (src == null)
			return null;
		Bitmap r;
		switch (style) {
			case 1:
				r = createCenteredBitmap(src, 0);
				break;
			case 2:
				r = createCenteredBitmap(src, 30);
				break;
			default:
				float ratio = Float.valueOf(src.getWidth()) / Float.valueOf(src.getHeight());
				r = Bitmap.createScaledBitmap(src, (int) (THUMBNAIL_HEIGHT * ratio), THUMBNAIL_HEIGHT, false);
				break;
		}
		return r;
	}

	/**
	 * Make thumb
	 *
	 * @param data  original PNG data
	 * @param style {@link ThumbImage#THUMB_STYLE_WRAP} {@link ThumbImage#THUMB_STYLE_ROUNDED}
	 * @return byte array
	 */
	public static byte[] makePNGThumb(byte[] data, int style) {
		Bitmap r = makeThumb(data, style);
		if (r == null)
			return null;
		ByteArrayOutputStream os;
		try {
			os = new ByteArrayOutputStream();
			r.compress(Bitmap.CompressFormat.PNG, 100, os);
			return os.toByteArray();
		} catch (Exception e) {
			Log.e(TAG, "makePNGThumb " + e.toString());
		}
		return null;
	}

	/**
	 * Creates centered bitmap {@link ThumbImage#THUMB_STYLE_WRAP}
	 *
	 * @param imageBitmap Bitmap
	 * @param radius      pixels
	 * @return Bitmap
	 */
	private static Bitmap createCenteredBitmap(Bitmap imageBitmap, int radius) {
		Bitmap r = Bitmap.createBitmap(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(r);
		int left, top, right, bottom, ofs;
		int w = imageBitmap.getWidth();
		int h = imageBitmap.getHeight();
		if (w > h) {
			ofs = (w - h) / 2;
			left = ofs;
			top = 0;
			right = h + ofs;
			bottom = h;
		} else {
			ofs = (h - w) / 2;
			left = 0;
			top = ofs;
			right = w;
			bottom = w + ofs;
		}
		Rect src = new Rect(left, top, right, bottom);
		Rect dst = new Rect(0, 0, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
		if (radius <= 0)
			c.drawBitmap(imageBitmap, src, dst, null);
		else {
			float scale;
			float dx = 0;
			float dy = 0;

			Matrix mShaderMatrix = new Matrix();
			if (w > h) {
				scale = (float) THUMBNAIL_HEIGHT / (float) h;
				dx = (THUMBNAIL_HEIGHT - w * scale) * 0.5f;
			} else {
				scale = (float) THUMBNAIL_HEIGHT / (float) w;
				dy = (THUMBNAIL_HEIGHT - h * scale) * 0.5f;
			}
			// Log.e("*****", "Scale: " + scale + " w:" + w + " h:" + h + " dx:" + dx + " dy:" + dy);

			mShaderMatrix.setScale(scale, scale);
			mShaderMatrix.postTranslate((int) (dx + 0.5f), (int) (dy + 0.5f));

			BitmapShader mBitmapShader = new BitmapShader(imageBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
			mBitmapShader.setLocalMatrix(mShaderMatrix);

			Paint bitmapPaint = new Paint();
			bitmapPaint.setStyle(Paint.Style.FILL);
			bitmapPaint.setAntiAlias(true);
			bitmapPaint.setShader(mBitmapShader);
			c.drawRoundRect(new RectF(0, 0, THUMBNAIL_HEIGHT, THUMBNAIL_HEIGHT),
					radius, radius, bitmapPaint);
		}
		return r;
	}

	/**
	 * Return thumb file name for PNG data (same as path parameter) or null
	 *
	 * @param path  thumb file name path
	 * @param data  PNG data
	 * @param style {@link ThumbImage#THUMB_STYLE_WRAP} or {@link ThumbImage#THUMB_STYLE_ROUNDED}
	 * @return path to the thumb. null if data is not PNG data
	 */
	public static String makeThumb(String path, byte[] data, int style) {
		File f = new File(path);
		if (f.exists())
			return path;
		Bitmap thumb = makeThumb(data, style);
		if (thumb != null) {
			FileOutputStream os;
			try {
				os = new FileOutputStream(f);
				thumb.compress(Bitmap.CompressFormat.PNG, 100, os);
				os.close();
				return path;
			} catch (Exception e) {
				Log.e(TAG, "makeThumb(3) " + e.toString());
			}
		}
		return null;
	}

	/**
	 * Return thumb file name for PNG data (same as path parameter) or null
	 *
	 * @param path  thumb file name path
	 * @param data  PNG data
	 * @param style {@link ThumbImage#THUMB_STYLE_WRAP} or {@link ThumbImage#THUMB_STYLE_ROUNDED}
	 * @return path to the thumb. null if data is not PNG data
	 */
	public static Bitmap makeThumbBitmap(String path, byte[] data, int style, boolean force) {
		File f = new File(path);
		if (f.exists() && (!force))
			return BitmapFactory.decodeFile(f.getAbsolutePath());
		Bitmap thumb = makeThumb(data, style);
		if (thumb != null) {
			FileOutputStream os;
			try {
				os = new FileOutputStream(f);
				thumb.compress(Bitmap.CompressFormat.PNG, 100, os);
				os.close();
			} catch (Exception e) {
				thumb = null;
				Log.e(TAG, "makeThumbBitmap" + e.toString());
			}
		}
		return thumb;
	}

	/**
	 * Return compressed bitmap as PNG byte array.
	 *
	 * @param value bitmap to read
	 * @return PNG byte array
	 * @see #makeThumb(Context, Uri, int)
	 */
	public static byte[] bitmap2PNGByteArray(Bitmap value) {
		byte[] r = null;
		ByteArrayOutputStream os;
		try {
			os = new ByteArrayOutputStream();
			bitmap2PNGOutputStream(value, os);
			r = os.toByteArray();
			os.close();
		} catch (Exception e) {
			Log.e(TAG, "bitmap2PNGArray " + e.toString());
		}
		return r;
	}

	/**
	 * Compress bitmap as PNG
	 *
	 * @param value Bitmap
	 * @param os    output stream
	 */
	private static void bitmap2PNGOutputStream(Bitmap value, OutputStream os) {
		try {
			value.compress(Bitmap.CompressFormat.PNG, 100, os);
		} catch (Exception e) {
			Log.e(TAG, "bitmap2PNGOutputStream " + e.toString());
		}
	}

	/**
	 * Compress bitmap as PNG to file
	 *
	 * @param value    Bitmap
	 * @param filename file name
	 * @return Uri
	 */
	public static Uri bitmap2PNGFile(Bitmap value, String filename) {
		File f = new File(filename);
		Uri r = null;
		try {
			FileOutputStream fos = new FileOutputStream(f);
			value.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
			r = Uri.parse(filename);
		} catch (Exception e) {
			Log.e(TAG, "bitmap2PNGFile " + e.toString());
		}
		return r;
	}

	/**
	 * Get PNG thumb as byte array for specified bitmap
	 *
	 * @param b     bitmap
	 * @param style {@link ThumbImage#THUMB_STYLE_WRAP} or {@link ThumbImage#THUMB_STYLE_ROUNDED}
	 * @return PNG thumb byte array
	 */
	public static byte[] getThumb(Bitmap b, int style) {
		byte r[] = bitmap2PNGByteArray(b);
		if (r == null)
			return null;
		Bitmap bmp = makeThumb(r, style);
		if (bmp == null)
			return null;
		r = bitmap2PNGByteArray(bmp);
		return r;
	}

	/**
	 * Get PNG thumb as byte array for specified bitmap uri.
	 *
	 * @param context Context used to load uri
	 * @param uri     PNG Uri
	 * @param style   {@link ThumbImage#THUMB_STYLE_WRAP} or {@link ThumbImage#THUMB_STYLE_ROUNDED}
	 * @return PNG byte array
	 */
	public static byte[] getThumb(Context context, Uri uri, int style) {
		// TODO
		Bitmap bmp = makeThumb(context, uri, style);
		if (bmp == null)
			return null;
		byte[] r = bitmap2PNGByteArray(bmp);
		bmp.recycle();
		return r;
	}

	/**
	 * Make PNG thumb
	 *
	 * @param filename File name
	 * @param photo    Bitmap
	 * @param style    {@link ThumbImage#THUMB_STYLE_WRAP} or {@link ThumbImage#THUMB_STYLE_ROUNDED}
	 * @return Uri to file
	 */
	public static Uri makeThumb(String filename, Bitmap photo, int style) {
		String p = makeThumb(filename, bitmap2PNGByteArray(photo), style);
		if (p != null) {
			try {
				return Uri.parse("file://" + p);
			} catch (Exception e) {
				Log.e(TAG, "makeThumb(3-)" + e.toString());
			}
		}
		return null;
	}
}