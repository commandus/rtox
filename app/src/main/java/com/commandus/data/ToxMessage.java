package com.commandus.data;

import android.content.ContentValues;
import android.database.Cursor;

import com.commandus.provider.MessageProvider;

public class ToxMessage {
    private long id;
    private int op;
    private int sent;
    private String userId;
    private String CN;
    private int messageType;
    private String body;
    private int tag;

    public ToxMessage() {
        this.id = 0;
        this.op = 0;
        this.sent = 0;
        this.userId = "";
        this.CN = "";
        this.messageType = 0;
        this.body = "";
        this.tag = 0;
    }

    public ToxMessage(long id, int op, int sent, String userId, String CN, int messageType, String body, int tag) {
        this.id = id;
        this.op = op;
        this.sent = sent;
        this.userId = userId;
        this.CN = CN;
        this.messageType = messageType;
        this.body = body;
        this.tag = tag;
    }

    /**
     * Contruct from for provider {@link com.commandus.provider.MessageProvider}
     * @param value value
     */
    public ToxMessage(ContentValues value) {
        this.id = value.getAsLong(MessageProvider.F_ID);
        this.op = value.getAsInteger(MessageProvider.F_OP);
        this.sent = value.getAsInteger(MessageProvider.F_SENT);
        this.userId = value.getAsString(MessageProvider.F_USER_ID);
        this.CN = value.getAsString(MessageProvider.F_USER_CN);
        this.messageType = value.getAsInteger(MessageProvider.F_MESSAGE_TYPE);
        this.body = value.getAsString(MessageProvider.F_BODY);
        this.tag = value.getAsInteger(MessageProvider.F_TAG);
    }

    /**
     * Contruct from for cursor
     * @param value value
     */
    public ToxMessage(Cursor value) {
        this.id = value.getLong(MessageProvider.ID);
        this.op = value.getInt(MessageProvider.OP);
        this.sent = value.getInt(MessageProvider.SENT);
        this.userId = value.getString(MessageProvider.USER_ID);
        this.CN = value.getString(MessageProvider.USER_CN);
        this.messageType = value.getInt(MessageProvider.MESSAGE_TYPE);
        this.body = value.getString(MessageProvider.BODY);
        this.tag = value.getInt(MessageProvider.TAG);
    }

    /**
     * Extract data for provider {@link com.commandus.provider.MessageProvider}
     * @param value value
     */
    public static ContentValues toContentValues(ToxMessage value) {
        ContentValues r = new ContentValues();
        r.put(MessageProvider.F_ID, value.getId());
        r.put(MessageProvider.F_OP, value.getOp());
        r.put(MessageProvider.F_SENT, value.getSent());
        r.put(MessageProvider.F_USER_ID, value.getUserId());
        r.put(MessageProvider.F_USER_CN, value.getCN());
        r.put(MessageProvider.F_MESSAGE_TYPE, value.getMessageType());
        r.put(MessageProvider.F_BODY, value.getBody());
        r.put(MessageProvider.F_TAG, value.getTag());
        return r;
    }

    public long getId() {
        return id;
    }

    public int getOp() {
        return op;
    }

    public int getSent() {
        return sent;
    }

    public String getUserId() {
        return userId;
    }

    public String getCN() {
        return CN;
    }

    public int getMessageType() {
        return messageType;
    }

    public String getBody() {
        return body;
    }

    public int getTag() {
        return tag;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setOp(int op) {
        this.op = op;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setCN(String CN) {
        this.CN = CN;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

}
