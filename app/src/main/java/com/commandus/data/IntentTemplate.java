package com.commandus.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Vector;

/**
 * Intent template class
 */
public class IntentTemplate implements Parcelable {
    private static final char SEPARATOR_1 = '|';
    private static final char SEPARATOR_LIST = ',';
    private String templateString;
    private String name;
    private String lang;
    private String description;
    private Vector<VariableColumn> variableColumnList;
    private String imageUri;
    private int id; // database only

    public IntentTemplate() {
        this.variableColumnList = new Vector<VariableColumn>();
    }

    protected IntentTemplate(Parcel in) {
        templateString = in.readString();
        name = in.readString();
        lang = in.readString();
        description = in.readString();
        in.readList(variableColumnList, null);
        imageUri = in.readString();
    }

    public static final Creator<IntentTemplate> CREATOR = new Creator<IntentTemplate>() {
        @Override
        public IntentTemplate createFromParcel(Parcel in) {
            return new IntentTemplate(in);
        }

        @Override
        public IntentTemplate[] newArray(int size) {
            return new IntentTemplate[size];
        }
    };

    public String getTemplateString() {
        return templateString == null ? "" : templateString;
    }

    public void setTemplateString(String templateString) {
        this.templateString = templateString;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLang() {
        return lang == null ? "" : lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getDescription() {
        return description == null ? "" : description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Vector<VariableColumn> getVariableColumnList() {
        return variableColumnList;
    }

    public void setVariableColumnList(Vector<VariableColumn> variableColumnList) {
        this.variableColumnList = variableColumnList;
    }

    public String getImageUri() {
        return imageUri == null ? "" : imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(templateString);
        dest.writeString(name);
        dest.writeString(lang);
        dest.writeString(description);
        dest.writeList(variableColumnList);
        dest.writeString(imageUri);
    }

    /**
     * Return string ready to parse with {@link IntentTemplateHelper#parse(String)}
     * Separators: '|'- intent template attrs, ','- VariableColumn list separator, ':'- VariableColumn attribute separator
     * where 0- lang 1-image uri 2- name, 3- description, 4- template string, 5..- "," separated column:variable:name:len:e index pairs array
     * lang|img|name|desc|template|column:variable:name:len:e,... e.g. 0:15:0:0:e,4:16:0:0:e
     * Special intent extra e.g.
     *      S.com.commandus.intent.fix.array=android.intent.extra.EMAIL,android.intent.extra.SUBS
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append(getLang()).append(SEPARATOR_1)
                .append(getImageUri()).append(SEPARATOR_1)
                .append(getName()).append(SEPARATOR_1)
                .append(getDescription()).append(SEPARATOR_1)
                .append(getTemplateString()).append(SEPARATOR_1);
        int sz1 = getVariableColumnList().size() - 1;
        int i = 0;
        for (VariableColumn vc: getVariableColumnList()) {
            b.append(vc.toString());
            if (i < sz1)
                b.append(SEPARATOR_LIST);
            i++;
        }
        return b.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int value) {
        id = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IntentTemplate that = (IntentTemplate) o;

        if (id != that.id) return false;
        if (templateString != null ? !templateString.equals(that.templateString) : that.templateString != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (lang != null ? !lang.equals(that.lang) : that.lang != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null)
            return false;
        if (variableColumnList != null ? !variableColumnList.equals(that.variableColumnList) : that.variableColumnList != null)
            return false;
        return imageUri != null ? imageUri.equals(that.imageUri) : that.imageUri == null;

    }

    @Override
    public int hashCode() {
        int result = templateString != null ? templateString.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (lang != null ? lang.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (variableColumnList != null ? variableColumnList.hashCode() : 0);
        result = 31 * result + (imageUri != null ? imageUri.hashCode() : 0);
        result = 31 * result + id;
        return result;
    }
}
