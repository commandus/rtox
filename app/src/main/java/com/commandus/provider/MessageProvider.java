package com.commandus.provider;

import android.content.*;
import android.database.*;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import android.net.Uri;
import android.content.ContentUris;
import android.support.annotation.NonNull;

/**
 * Saved messages
 */
public class MessageProvider extends ContentProvider {
    private SQLiteDatabase mDB;

	private static final String TABLE = "msg";
	private static final String CURSOR_TYPE_DIR = AppProviderHelper.CURSOR_TYPE_DIR + AppProviderHelper.DOMAIN + "." + TABLE;
	private static final String CURSOR_TYPE_ITEM = AppProviderHelper.CURSOR_TYPE_ITEM + AppProviderHelper.DOMAIN + "." + TABLE;

	public static final Uri CONTENT_URI = Uri.parse(AppProviderHelper.PROTO_CONTENT + AppProviderHelper.DOMAIN + "." + TABLE);

	public static final String CONTENT_AUTHORITY = AppProviderHelper.DOMAIN + "." + TABLE;

	//
    public static final int OP_RECV = 0;
    public static final int OP_SENT = 1;

    // fields
	private static final String F_IID = "_id";
	// message identifier
    public static final String F_ID  = "id";
    public static final String F_OP = "op";				// Unix epoch time
    public static final String F_SENT = "sent";			// Unix epoch time
    public static final String F_USER_ID = "u_id";
    public static final String F_USER_CN = "u_cn";
    public static final String F_MESSAGE_TYPE = "t";
    public static final String F_BODY = "body";
    public static final String F_TAG = "tag";

	public static final int ID = 1;
	public static final int OP = 2;
	public static final int SENT = 3;
	public static final int USER_ID = 4;
	public static final int USER_CN = 5;
    public static final int MESSAGE_TYPE = 6;
	public static final int BODY = 7;
	public static final int TAG = 8;

	private static final UriMatcher URL_MATCHER;
	private static class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper(Context context) {
			super(context, TABLE, null, AppProviderHelper.DATABASE_VERSION);
		}
		@Override public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + TABLE + " (" + F_IID + AppProviderHelper.TYPE_PK_COLON
					+ F_ID + AppProviderHelper.TYPE_INTEGER_COLON
					+ F_OP + AppProviderHelper.TYPE_INTEGER_COLON
					+ F_SENT + AppProviderHelper.TYPE_INTEGER_COLON
					+ F_USER_ID + AppProviderHelper.TYPE_TEXT_COLON
					+ F_USER_CN + AppProviderHelper.TYPE_TEXT_COLON
                    + F_MESSAGE_TYPE + AppProviderHelper.TYPE_INTEGER_COLON
					+ F_BODY + AppProviderHelper.TYPE_BLOB_COLON
					+ F_TAG + AppProviderHelper.TYPE_INTEGER
					+")");
			db.execSQL(AppProviderHelper.getStmtCreateIndex1(TABLE, F_ID));
		}
		@Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL(AppProviderHelper.getStmtDropIndex1(TABLE, F_ID));
			db.execSQL(AppProviderHelper.STMT_DROP_TABLE + TABLE);
			onCreate(db);
		}
	}

	@Override
	public boolean onCreate() {
		DatabaseHelper dbHelper = new DatabaseHelper(getContext());
		mDB = dbHelper.getWritableDatabase();
		return (mDB != null);
	}

	@Override
	public Cursor query(@NonNull Uri url, String[] projection, String selection, String[] selectionArgs, String sort) {
		switch (URL_MATCHER.match(url)) {
			case 1:
				if ((sort == null) || (sort.isEmpty())) {
					sort = F_IID + " DESC";
				}
				break;
			case 2:
				selection = F_ID + "=?";
				selectionArgs = new String[]{url.getPathSegments().get(0)};
				break;
			default:
				throw new IllegalArgumentException(AppProviderHelper.ERR_WRONG_URL + url);
		}
		Cursor c = mDB.query(TABLE, null, selection, selectionArgs, null, null, sort);
		Context ctx = getContext();
		if (ctx != null)
			c.setNotificationUri(ctx.getContentResolver(), url);
		return c;
	}

	@Override
	public String getType(@NonNull Uri url) {
		switch (URL_MATCHER.match(url)) {
			case 1:
				return CURSOR_TYPE_DIR;
			case 2:
				return CURSOR_TYPE_ITEM;
			default:
				throw new IllegalArgumentException(AppProviderHelper.ERR_WRONG_URL + url);
		}
	}

	/**
	 * Find certificate by cert id
	 * @param certId certificate identifier
	 * @return row id
	 */
	private long findRowId(String certId)
	{
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(TABLE);
		Cursor c = qb.query(mDB, new String[]{F_ID}, F_ID + "=?", new String[]{certId},
				null, null, null);
		long r = 0;
		if (c.moveToFirst())
			r = c.getLong(0);
		c.close();
		return r;
	}

	@Override
	public Uri insert(@NonNull Uri url, ContentValues initialValues) {
		if (URL_MATCHER.match(url) < 1)
			throw new IllegalArgumentException(AppProviderHelper.ERR_WRONG_URL + url);

		ContentValues values;
		if (initialValues != null)
			values = new ContentValues(initialValues);
		else
			values = new ContentValues();

		if (!values.containsKey(F_ID))
			throw new IllegalArgumentException(AppProviderHelper.ERR_NO_ID + url);

		long row = findRowId(values.getAsString(F_ID));
		if (row == 0)
			row = mDB.insert(TABLE, F_BODY, values);
		else
			mDB.update(TABLE, values, F_ID + "=?", new String[]{Long.toString(row)});
		Uri uri = ContentUris.appendId(CONTENT_URI.buildUpon(), row).build();
		Context ctx = getContext();
		if (ctx != null)
			ctx.getContentResolver().notifyChange(uri, null);
		return uri;
	}

	@Override public int delete(@NonNull Uri url, String where, String[] whereArgs) {
		int count;
		switch (URL_MATCHER.match(url)) {
			case 1:
				count = mDB.delete(TABLE, where, whereArgs);
				break;
			case 2:
				String segment = url.getPathSegments().get(0);
				count = mDB.delete(TABLE, F_ID + "=?", new String[] {segment});
				break;
			default:
				throw new IllegalArgumentException(AppProviderHelper.ERR_WRONG_URL + url);
		}
		Context ctx = getContext();
		if (ctx != null)
			ctx.getContentResolver().notifyChange(url, null);
		return count;
	}

	@Override
	public int update(@NonNull Uri url, ContentValues values, String where, String[] whereArgs) {
		int count;
		switch (URL_MATCHER.match(url)) {
			case 1:
				count = mDB.update(TABLE, values, where, whereArgs);
				break;
			case 2:
				String segment = url.getPathSegments().get(0);
				count = mDB.update(TABLE, values, F_ID + "=?", new String[] {segment});
				if (count == 0)
					insert(url, values);
				break;
			default:
				throw new IllegalArgumentException(AppProviderHelper.ERR_WRONG_URL + url);
		}
		Context ctx = getContext();
		if (ctx != null)
			ctx.getContentResolver().notifyChange(url, null);
		return count;
	}

	static {
		URL_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
		URL_MATCHER.addURI(CONTENT_AUTHORITY, null, 1);
		URL_MATCHER.addURI(CONTENT_AUTHORITY, "#", 2);
	}
}
