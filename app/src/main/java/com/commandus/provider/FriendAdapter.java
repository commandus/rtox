package com.commandus.provider;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.commandus.rtox.R;
import com.commandus.rtox.ToxClient;

import java.util.Map;

class FriendAdapter extends BaseAdapter {

    private ToxClient mClient;
    private Map<String, String> friendList;

    FriendAdapter(ToxClient client) {
        mClient = client;
        friendList = client.getFriends();
    }

    @Override
    public int getCount() {
        if (friendList == null)
            return 0;
        return friendList.size();
    }

    @Override
    public Object getItem(int position) {
        if (friendList == null)
            return null;
        if (position < 0 || position >= friendList.size())
            return null;
        return friendList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item_friend, parent, false);
        }
        TextView tv_cn = convertView.findViewById(R.id.text_friend_cn);
        if (friendList == null)
            return convertView;
        if (position < 0 || position >= friendList.size())
            return convertView;
        tv_cn.setText(friendList.get(position));
        return convertView;
    }

}
