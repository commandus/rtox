package com.commandus.provider;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.commandus.data.ToxMessage;
import com.commandus.rtox.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MessageAdapter extends SimpleCursorAdapter {
    private Context mContext;

    public MessageAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();

        Cursor c = (Cursor) getItem(position);

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (vi != null) {
                convertView = vi.inflate(R.layout.list_item_message, null);
            }
        }
        if (c == null)
            return convertView;
        ToxMessage m = new ToxMessage(c);

        TextView txtMessage = convertView.findViewById(R.id.txtMessage);
        TextView txtInfo = convertView.findViewById(R.id.txtInfo);

        txtMessage.setText(m.getBody());
        boolean isMine = m.getOp() != 0;
        setChatViewLocalOrRemote(convertView, isMine);

        Date dt = new Date(m.getSent() * 1000L);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);

        SimpleDateFormat sdf;
        if (dt.before(calendar.getTime()))
            sdf = new SimpleDateFormat(context.getString(R.string.fmtDateLongChat));
        else
            sdf = new SimpleDateFormat(context.getString(R.string.fmtDateShortChat));
        txtInfo.setText(sdf.format(dt));
        return convertView;
    }

    /**
     * Used by {@link #getView(int, View, ViewGroup)}
     * @param convertView view
     * @param isMine true- local (me), false- remote (respondent)
     */
    private void setChatViewLocalOrRemote(View convertView, boolean isMine) {
        LinearLayout contentChatMsg = convertView.findViewById(R.id.contentChatMsg);
        LinearLayout contentWithBackground = convertView.findViewById(R.id.contentWithBackground);
        TextView txtMessage = convertView.findViewById(R.id.txtMessage);
        TextView txtInfo = convertView.findViewById(R.id.txtInfo);
        LinearLayout.LayoutParams lpBackground = (LinearLayout.LayoutParams) contentWithBackground.getLayoutParams();
        RelativeLayout.LayoutParams lpContent = (RelativeLayout.LayoutParams) contentChatMsg.getLayoutParams();
        LinearLayout.LayoutParams lpMsg = (LinearLayout.LayoutParams) txtMessage.getLayoutParams();
        LinearLayout.LayoutParams lpInfo = (LinearLayout.LayoutParams) txtInfo.getLayoutParams();
        if (isMine) {
            // out, myself
            lpContent.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lpContent.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            contentChatMsg.setLayoutParams(lpContent);

            contentWithBackground.setBackgroundResource(R.drawable.chatmsg_out_bg);
            lpBackground.gravity = Gravity.END;	// Gravity.RIGHT;
            contentWithBackground.setLayoutParams(lpBackground);

            lpMsg.gravity = Gravity.START; // Gravity.LEFT;
            txtMessage.setLayoutParams(lpMsg);

            lpInfo.gravity = Gravity.END; // Gravity.RIGHT;
            txtInfo.setLayoutParams(lpInfo);
        } else {
            // in
            // lpContent.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            // lpContent.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            lpContent.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lpContent.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            contentChatMsg.setLayoutParams(lpContent);

            contentWithBackground.setBackgroundResource(R.drawable.chatmsg_in_bg);
            lpBackground.gravity = Gravity.START; // Gravity.LEFT;
            contentWithBackground.setLayoutParams(lpBackground);

            lpMsg.gravity = Gravity.START;	// Gravity.LEFT
            txtMessage.setLayoutParams(lpMsg);

            lpInfo.gravity = Gravity.END; // Gravity.RIGHT
            txtInfo.setLayoutParams(lpInfo);
        }
    }
}
