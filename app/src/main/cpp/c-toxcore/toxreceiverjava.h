#ifndef TOXRECEIVERJAVA_H
#define TOXRECEIVERJAVA_H
#ifdef __ANDROID__
#include <jni.h>
#include <android/log.h>

#include <inttypes.h>
#include <iostream>
#include <queue>
#include <tox/tox.h>
#include "toxreceiver.h"
#include "toxclient.h"
#include "toxmessage.h"

class ToxReceiverJava : public ToxReceiver
{
private:
    JNIEnv *jenv;
    jclass jcls;
    jobject jobj;
	jmethodID jOnId;
	jmethodID jOnConnectionStatus;
	jmethodID jOnMessage;
	jmethodID jOnFriendRequest;

	std::queue<ToxMessage> messages;
	TOX_MESSAGE_TYPE defaultMessageType;
public:
	ToxReceiverJava
	(
        JNIEnv *env,
        jobject obj,
		TOX_MESSAGE_TYPE default_message_type
	);
    virtual ~ToxReceiverJava();

	virtual void onId(
		ToxClient *toxclient,
		const std::string &valuehex
	);
	virtual void onConnectionStatus(
		ToxClient *toxclient,
		TOX_CONNECTION value
	);
	virtual void onMessage
	(
		ToxClient *toxclient,
        TOX_MESSAGE_TYPE message_type,
		uint32_t friend_number, 
		const std::string &value,
		void *user_data
	);
	virtual bool onFriendRequest(
		ToxClient *toxclient,
		const uint8_t *key, 
		const std::string &name,
		void *user_data
	);

	// do not reflect to Java
	virtual bool nextMessageTo
	(
		uint32_t *friend_number,
		TOX_MESSAGE_TYPE *message_type,
		std::string *m
	);

	// do not reflect to Java. Called from putMessage reflected in ToxClient
    virtual void putMessage
	(
			ToxClient *toxclient,
            const TOX_MESSAGE_TYPE message_type,
			const uint32_t friend_number,
			const std::string &value
	);
};

#endif
#endif
