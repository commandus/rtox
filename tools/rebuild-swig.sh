#!/bin/sh
cd ~/src/rtox/app/src/main/cpp/sodium
swig -java -c++ -package org.abstractj.sodium -o sodiumJAVA_wrap.cxx -outdir ../../../../src/main/java/org/abstractj/sodium/ sodium.i
cd ~/src/rtox/app/src/main/cpp/toxcore
swig -java -c++ -package org.irungentoo.toxcore -o toxcoreJAVA_wrap.cxx -outdir ../../../../src/main/java/org/irungentoo/toxcore/ toxcore.i
cd ~/src/rtox/
